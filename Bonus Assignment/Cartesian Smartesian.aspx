﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cartesian Smartesian.aspx.cs" Inherits="Bonus_Assignment.Cartesian_Smartesian" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>Find the quadrant where your values for x and y lies</h2>
        </div>
        <div>
            <label>Enter the value for x axis:</label><br />
            <asp:TextBox runat="server" ID="InputX"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Validator1" ControlToValidate="InputX" Display="Static" ForeColor="Red" ErrorMessage="Value for x is required" runat="server" />
            
        </div>
        <div>
            <label>Enter the value for y axis:</label><br />
            <asp:TextBox runat="server" ID="InputY"></asp:TextBox>
             <asp:RequiredFieldValidator ID="Validator2" ControlToValidate="InputY" Display="Static" ForeColor="Red" ErrorMessage="Value for y is required" runat="server" />
            
            <p>
                <asp:Button ID="button" runat="server" OnClick="button_click" Text="Check" />
            </p>
            <div id="output" runat="server">
            </div>
        </div>
    </form>
</body>
</html>
