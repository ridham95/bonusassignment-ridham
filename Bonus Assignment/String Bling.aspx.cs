﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus_Assignment
{
    public partial class String_Bling : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void button_click(object sender, EventArgs e)
        {
            string x, xrev = "";
            x = (Input1.Text).ToLower();
            x = x.Replace(" ", "");

            for(int j=x.Length-1;j>=0;j--)
            {
                xrev += x[j].ToString();
            }
            if(xrev== x)
            {
                output.InnerHtml = "This string is a palindrome";
            }
           else
            {
                output.InnerHtml = "This string is not a palindrome";
            }


        }
    }
}