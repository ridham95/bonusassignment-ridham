﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Prime Number.aspx.cs" Inherits="Bonus_Assignment.Prime_Number" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <div>
            <h2>Prime Check..!!</h2>
        </div>
        <div>
            <label>Enter Number:</label><br />
            <asp:TextBox runat="server" ID="Input1"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Validator1" ControlToValidate="Input1" Display="Static" ForeColor="Red" ErrorMessage="Number Required" runat="server" />
            
        </div>
              <p>
                <asp:Button ID="button" runat="server" OnClick="button_click" Text="Check" />
            </p>
            <div id="output" runat="server">
            </div>
        </div>
    </form>
</body>
</html>
