﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus_Assignment
{
    public partial class Cartesian_Smartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void button_click(object sender, EventArgs e)
        {
            int x, y;
            x = Convert.ToInt32(InputX.Text);
            y = Convert.ToInt32(InputY.Text);
            if(x>0 && y>0)
            {
                output.InnerHtml = "Lies in First Quadrant";
            }
            else if (x < 0 && y > 0)
            {
                output.InnerHtml = "Lies in Second Quadrant";
            }
            else if (x < 0 && y < 0)
            {
                output.InnerHtml = "Lies in Third Quadrant";
            }
            else if (x > 0 && y < 0)
            {
                output.InnerHtml = "Lies in Fourth Quadrant";
            }
            else
            {
                output.InnerHtml = "Lies on origin";
            }
            
        }
    }
}