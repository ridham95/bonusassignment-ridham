﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus_Assignment
{
    public partial class Prime_Number : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void button_click(object sender, EventArgs e)
        {
            int x;
            x = Convert.ToInt32(Input1.Text);
            bool y = true;
            for(int i=2;i<=x/2;i++)
            {
                if(x%2==0)
                {
                    y = false;
                    break;
                }
            }
            if(y)
            {
                output.InnerHtml = "Entered number is prime";
            }
            else
            {
                output.InnerHtml = "Entered number is not prime";
            }
        }
    }
}